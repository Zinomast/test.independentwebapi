module FSharpAPI.Controllers
open System
open System.Collections.Generic
open System.Linq
open System.Net.Http
open System.Web.Http
open Newtonsoft.Json
//open FSharpAPI.Models

[<CLIMutable>]
type Values = {
    Value: string
    Id : int
}

type ValuesController() =
    inherit ApiController()

    let values = [{Value = "value1"; Id = 1};{Value = "value2"; Id = 2}]
    member x.Get() = values
    member x.Get(id: int) = values |> List.filter(fun v -> v.Id = id)

