﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace OwinService.Controllers
{
	[RoutePrefix("api/testing")]
	public class RoutedController : ApiController
	{
		public IEnumerable<string> GetAllItems()
		{
			return new string[] { "value 1", "value 2" };
		}
	}
}
