﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using System.Web.Http;

[assembly: OwinStartup(typeof(OwinService.Startup))]

namespace OwinService
{
	public class Startup
	{
		//  Hack from http://stackoverflow.com/a/17227764/19020 to load controllers in 
		//  another assembly.  Another way to do this is to create a custom assembly resolver
		Type valuesControllerType = typeof(OwinService.Controllers.ValuesController);
		public void Configuration(IAppBuilder app)
		{
			// For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
			HttpConfiguration config = new HttpConfiguration();
			config.MapHttpAttributeRoutes();
			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);
			app.UseWebApi(config);
		}
	}
}
