﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Test.OwinWebAPI
{
	class Program
	{
		static void Main(string[] args)
		{
			string address = "http://localhost:9000/";
			using (WebApp.Start<Startup>(url: address))
			{
				HttpClient client = new HttpClient();
				var response = client.GetAsync(address + "api/values").Result;
				Console.WriteLine(response);
				Console.WriteLine(response.Content.ReadAsStringAsync().Result);
			}
			Console.ReadLine();
		}
	}
}
