﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.SelfHost;

namespace Test.WebAPI
{
	class Program
	{
		static void Main(string[] args)
		{
			var address = "http://localhost:900";
			var config = new HttpSelfHostConfiguration(address);
			config.MapHttpAttributeRoutes();
			using (var server = new HttpSelfHostServer(config))
			{
				server.OpenAsync().Wait();
				Console.WriteLine("Server running at {0}. Press any key to exit", address);
				Console.ReadLine();
			}
		}
	}
}
